<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\JsonResponse;

class NumerosController extends AbstractController
{
    /**
     * @Route("/numeros", name="numeros")
     */
    public function numeros()
    {
        $variables=array(3=>'Dining', 5=>'Edge');
        $arrayName = array();
        for($i=1;$i<100;$i++){
          $arrayName[$i]=$i;
          $var=0;
          foreach ($variables as $key => $value) {
             if($i % $key == 0 ){
              $arrayName[$i]=$value;
              $var++;
              switch($var){
                case 2:
                  $arrayName[$i]='DiningEdge';
                   break;
                default:break;
              }
             }
          }
        }
      //  return new JsonResponse(array('name' => $arrayName));
        return $this->render('numeros/index.html.twig', [
            'numeros' =>  $arrayName,
      ]);
    }
    /**
     * @Route("/numeros/api", name="numeros/api", methods={"GET","HEAD"})
     */
    public function APInumeros()
    {
      $variables=array(3=>'Dining', 5=>'Edge');
      $arrayName = array();
      for($i=1;$i<100;$i++){
        $arrayName[$i]=$i;
        $var=0;
        foreach ($variables as $key => $value) {
           if($i % $key == 0 ){
            $arrayName[$i]=$value;
            $var++;
            switch($var){
              case 2:
                $arrayName[$i]='DiningEdge';
                 break;
              default:break;
            }
           }
        }
      }
      return new JsonResponse(array('numeros' => $arrayName, 'Access-Control-Allow-Origin' => '*'));
       // $layout = $twig->load('numeros/index.html.twig');

     //   $twig->display('template.twig', ['layout' => $layout]);
        return $this->render('numeros/index.html.twig', [
            'controller_name' => 'NumerosController',
      ]);
    }
}
